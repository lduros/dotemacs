;; ---------
;; LongLines
;; ---------

;; act (more) like a word processor
(add-to-list 'auto-mode-alist '("\\.ll\\'" . text-mode))
(add-to-list 'auto-mode-alist '("\\.txt\\'" . text-mode))


(provide 'conf-text)
