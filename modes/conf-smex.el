;;; Code:

(require 'smex)


(setq smex-save-file (in-emacs-d ".cache/smex-items"))
(smex-initialize)

(provide 'conf-smex)
