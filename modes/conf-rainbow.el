;; Code:
(require 'rainbow-delimiters)
(require 'rainbow-identifiers)

(setq rainbow-delimiters-mode nil
      rainbow-identifiers-mode nil)

(provide 'conf-rainbow)
