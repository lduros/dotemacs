;;; Code:
(require 'buffer-defuns)
(require 'clj-defuns)
(require 'editing-defuns)
(require 'file-defuns)
(require 'js2r-defuns)
(require 'lisp-defuns)
(require 'misc-defuns)
(require 'nifty-defuns)
(require 'project-defuns)
(require 'window-defuns)
(require 'snippet-helpers)


(provide 'init-defuns)
