# Distopico Emacs Config
**Version 2.0.2**

My emacs configuration with many toys for programming environment mostly javascript , python , php, html
and tools for intent be more productive and social.

### Requirements ###
- Node.js v0.12 or higher
- Python 2.7 or 3.x
- Pip

Install
-------
```shell
  npm install

  virtualenv .virtualenv/$USER
  source .virtualenv/$USER/bin/activate
  pip install -r requirements.txt
```

License
-------

Licensed of coures under the GPL3 License - see the [LICENSE](LICENSE) file for details
